# TappedIn: A social networking site for martial artists 
## General Purpose

This is really a toy project because I need something to work on to improve my technical skills.

That said, the idea is to create a social networking site explicitly for people in the martial arts. It should allow people to review schools, instructors, and other students, and perhaps allow people to find other people to spar with or learn from.

Possibly some sort of forum system might be useful.

## Architectural and Learning Goals

The architextural goal of tapped in is to create an RESTFull API that can be consumed by multiple clients. So, for example, the tapped in API should work equally well for a Spine App as for IOS, Angular, or any other presentation layer. Ultimately, the purpose is to be able to swap out any part of the system, database, api, clients, and as long as all contracts are upheld the other parts of the system should not notice at all.

I expect to learn Rails API, Vagrant, Neo4J (again), and Angular, although specific technologies might change.

## Environments
### Development Environment

In order to present the best possible model of development for tapped in, we want to be able to copy a 'production like' environment for development testing. This will neccessitate a private vagrant network.

### Test Environment

Unclear at this point whether a separate testing environment will be needed, possibly there are online resources such as [Travis CI](https://travis-ci.org/) that will be a help here. At some point the whole project should be put onto some sort of CI system.

### Production Environment

Probably heroku, defferred.

### Source Control

Git
