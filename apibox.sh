#! /bin/bash

/etc/init.d/networking restart 
sudo apt-get update
sudo apt-get -y install ruby1.9.3
sudo apt-get -y install sqlite3 libsqlite3-dev
sudo gem install bundler
sudo gem install rails-api
