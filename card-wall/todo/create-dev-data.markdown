# Tech Task: create some data in the dev environment

## Definition

We need some example data in the database, so lets write a script that loads the data we need into neo4j. Initially, it would be good to just have a few users, we'll worry about the relationships between users later.
