# Narrative

*As a* martial arts instructor
*I want* to create an account
*So that* people can find my profile

# Acceptance Criteria

Given a user creates an account in the system
When a member of the general public goes to that users page
They should see the users public profile

# Out of scope

Searching for the users profile
