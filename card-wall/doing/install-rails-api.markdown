# Tech Task: create rails-api vagrant box

## Definition

The backend API will be rails-api, so we need to install rails api onto a vagrant box. We should be able to reach this box from the other vagrant boxes, particularly the client box.

## Technical notes

This was fairly straightforward. A vanilla box was used so that there would only be one version of ruby installed on the box, an shell provisioner sufficed to get everything installed.
