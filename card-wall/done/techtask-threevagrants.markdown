# Tech Task: create three vagrant boxes

## Definition

We need three vagrant boxes, each of which can ping the other, set up in a private network.

## Completion Notes:

3 machines have been configured.

db - 192.168.28.1
api - 192.168.28.2
client - 192.168.28.3
