# Technical Task: Install Neo4j

## Description

Install Neo4j on one of the vagrant boxes available. We should be able to reach this via wget or some other web client from the vagrant box for the API.

## Technical Notes 

This has turned out to be tricky. The Ubuntu box installs open-jdk by default, and this is making things fail. Installing the Oracle JDK worked (apparently) but I wasn't able to use port forwarding to see the neo4j server correctly. 

After a fair amount of searching, I found a blog post on [installing neo4j on vagrant using chef](http://thinkingonthinking.com/An-experiment-with-Vagrant-and-Neo4J/), complete with cookbook. This worked in a standalone folder, although vagrant reload had to be called at least once in order to bring up a machine that was serving Neo4j. The instructions at the blog post are fairly important. In particular: `librarian-chef init` and `librarian-chef install` to pick up the chef cookbooks.  

## Resolution

Tested by creating a few nodes then booting up the api server and using curl to connect to the neo4j server. Simple GET requests returned data.
